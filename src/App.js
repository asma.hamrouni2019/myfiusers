import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";

import Login from "./components/login";
import logo2 from "./logo2.jpg";

import logo from "./logo.svg";
import "./App.css";

import { Route, Switch, HashRouter } from "react-router-dom";
import Subscription from "./components/subscription";
import updatesubsc from "./components/updatesubsc";
import newcontract from "./components/newcontract";
import subscription from "./components/subscription";
class App extends Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route path="/" exact component={Subscription} />
          <Route path="/co" component={newcontract} />
          <Route path="update/:postId" component={updatesubsc} />

          {/* <Route path="/subs" component={Subscription} /> */}
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
