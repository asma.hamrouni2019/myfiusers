import React, { Component, useState } from "react";
import { Form, Button, Nav } from "react-bootstrap";
import logo2 from "./logo2.jpg";
import axios, { UploadScreen } from "axios";
import { Auth } from "aws-amplify";

import { Redirect, Link } from "react-router-dom";
class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: ""
    };
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = async event => {
    event.preventDefault();

    try {
      await Auth.signIn(this.state.userName, this.state.password);
      alert("Logged in");
    } catch (e) {
      alert(e.message);
    }
  };
  handleClick(event) {
    var apiBaseUrl = "127.0.0.1:5000/userlogin";
    var self = this;
    var payload = {
      email: this.state.userName,
      password: this.state.password
    };
    axios
      .post("http://127.0.0.1:5000/userlogin", null, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept"
        }
      })
      .then(res => console.log(res))
      .catch(error => console.log(error));
  }
  render() {
    return (
      <div
        className="aside col-sm-4 py-5 col-sm-offset-4 bg-primary  text-white text-center "
        style={{ margin: "auto", display: "block", marginTop: 40 }}
      >
        <div className=" ">
          <div className="card-body">
            <img src={logo2} alt="logo" style={{ width: "100%" }} />
          </div>
        </div>

        {/* <div
        className="col-sm-4 col-sm-offset-2 "
        style={{ margin: "auto", display: "block", marginTop: 40 }}
      /> */}
        <Form style={{ width: "100%" }}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>User Name</Form.Label>
            <Form.Control
              type="userName"
              placeholder="Enter your user name"
              onChange={this.handleChange}
            />
            <Form.Text className="text-muted">
              We'll never share your user name with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button
            variant="primary"
            type="submit"
            // onClick={this.handleSubmit}
            onClick={event => this.handleClick(event)}
          >
            Sign in
          </Button>
          <Link to="/subs">
            <Button variant="link" type="button" className="d-block mx-auto">
              Create new Account
            </Button>
          </Link>
        </Form>
      </div>
    );
  }
}

export default login;
