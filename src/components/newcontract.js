import React, { Component } from "react";
import logo3 from "./logo3.png";
import subscription from "./subscription";
export default class newcontract extends Component {
  constructor(props) {
    super(props);
    var today = new Date(),
      date =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();

    this.date = date;
  }

  componentDidMount() {
    {
      window.print();
    }
    fetch("http://95.0.46.234:5454/api/GetContractId/userName")
      .then(results => results.json())
      .then(json => {
        console.log(json);
        this.setState({ userName: json });
      });
  }

  render() {
    return (
      <div className="container">
        {/*---- Include the above in your HEAD tag --------*/}
        <div className="container">
          <div className="row">
            <div className="card-body">
              <div className="card-img-top">
                <img
                  src={logo3}
                  alt="logo"
                  style={{ width: "15%" }}
                  className="pull-left"
                />
              </div>
              <div className="invoice-title">
                <h1
                  style={{
                    fontSize: 30,
                    textAlign: "center",
                    fontFamily: "Verdana"
                  }}
                >
                  ABONELİK SÖZLEŞMESİ
                </h1>
              </div>

              <hr />
              <div className="row">
                <div className="col-md-6 text-left">
                  Sözleşme Kimliği
                  <br />
                </div>
                <div className="col-md-6 text-right">
                  <address>
                    Tarih:
                    <strong>
                      <p>{this.date}</p>
                    </strong>
                  </address>
                </div>
              </div>
              <div className="mb-5">
                Hi̇zmet no
                <p>{JSON.parse(localStorage.getItem("servicePacket"))}</p>
                {console.log(JSON.parse(localStorage.getItem("servicePacket")))}
              </div>

              <div>
                <address>
                  Adres
                  <br />
                  {JSON.parse(localStorage.getItem("streetnumber"))}
                  {JSON.parse(localStorage.getItem("street"))} {","}
                  {JSON.parse(localStorage.getItem("neighborhood"))}
                  {JSON.parse(localStorage.getItem("district"))}
                  {JSON.parse(localStorage.getItem("poste"))}
                  {JSON.parse(localStorage.getItem("city"))}
                  <br />
                </address>
              </div>
            </div>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading">
              <h3
                className="mb-4"
                style={{ textAlign: "center", fontFamily: "Verdana" }}
              >
                ABONE BİLGİLERİ
              </h3>
            </div>
            <div className="panel-body">
              <div className="table-responsive">
                <table className="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <td>Adı Soyadı :</td>
                      <td>
                        {/* style="width:124px;" */}{" "}
                        {JSON.parse(localStorage.getItem("LastName"))}{" "}
                        {JSON.parse(localStorage.getItem("FirstName"))}
                      </td>

                      <td>Cinsiyet:</td>
                      <td>{JSON.parse(localStorage.getItem("gender"))}</td>
                    </tr>
                  </thead>
                  <tbody>
                    {/* foreach ($order->lineItems as $line) or some such thing here */}
                    <tr>
                      <td>Ki̇mli̇k No:</td>
                      <td>{JSON.parse(localStorage.getItem("idcard"))}</td>
                      <td>Pasaport No :</td>
                      <td>{JSON.parse(localStorage.getItem("passport"))}</td>
                    </tr>
                    <tr>
                      <td>Cep Telefonu:</td>
                      <td>{JSON.parse(localStorage.getItem("Telephone"))}</td>
                      <td>Sabi̇t Telefon:</td>
                      <td>{JSON.parse(localStorage.getItem("GSM"))}</td>
                    </tr>
                    <tr>
                      <td>Posta Kodu:</td>
                      <td>{JSON.parse(localStorage.getItem("poste"))}</td>
                      <td>İlçe:</td>
                      <td>{JSON.parse(localStorage.getItem("district"))}</td>
                    </tr>

                    <tr>
                      <td>İl:</td>
                      <td>{JSON.parse(localStorage.getItem("neighbrhood"))}</td>
                      <td>Faks :</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Vergi Dairesi :</td>
                      <td>{JSON.parse(localStorage.getItem("txid"))}</td>
                      <td>Durum :</td>

                      <td>{JSON.parse(localStorage.getItem("status"))}</td>
                    </tr>
                    <tr>
                      <td>Email :</td>
                      <td>{JSON.parse(localStorage.getItem("Email"))}</td>
                      <td>E-posta :</td>
                      {/* // {JSON.parse(localStorage.getItem("tc"))} */}
                      <td> </td>
                    </tr>
                    {/* 
                        <tr>
                          <td className="no-line" />
                          <td className="no-line" />
                          <td className="no-line text-center">
                            <strong>Shipping</strong>
                          </td>
                          <td className="no-line text-right">$15</td>
                        </tr>
                        <tr>
                          <td className="no-line" />
                          <td className="no-line" />
                          <td className="no-line text-center">
                            <strong>Total</strong>
                          </td>
                          <td className="no-line text-right">$685.99</td>
                        </tr> */}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div></div>
        <br></br>
        <br></br>
        <br></br>
        <div class="pagebreak mb-4"> </div>

        <strong style={{ fontSize: 22 }} className="mb-4">
          9-SÖZLEŞMENİN NÜSHALARI , TARİHİ VE İMZALANDIĞI YER{" "}
        </strong>

        <p style={{ fontSize: 20 }}>
          İşbu sözleşme, Servis Sağlayıcı ( MYFİ ) ve Abone’de ayrı ayrı olmak
          üzere 2(iki) nüsha olarak <br></br>
          <strong>
            <p>{this.date}</p>
          </strong>
          tarihinde{" "}
          <strong>
            {" "}
            {JSON.parse(localStorage.getItem("district"))} {"  "}
            {JSON.parse(localStorage.getItem("neighborhood"))} {","}{" "}
            {JSON.parse(localStorage.getItem("streetnumber"))}{" "}
            {JSON.parse(localStorage.getItem("street"))}{" "}
          </strong>
          adresinde akdedilmiştir. Yukarıda verdiğim bilgilerin doğru ve tam
          olduğunu beyan eder, işbu beyanın sözleşmenin ayrılmaz bir
          parçasıolduğunu, seçmiş olduğum TARİFE/PAKET’e ilişkin MYFİ tarafından
          tam olarak bilgilendirildiğimi; işbu sözleşmeyi imzalamadan önce, tüm
          maddelerini okuduğumu, anladığımı, kabul ettiğimi beyan ederim.
        </p>

        <div className="row" style={{ "margin-top": "400px" }}>
          <div className="col-md-6 text-left">
            <strong style={{ fontSize: 14 }}>ABONE</strong>
            <br />
            <strong>
              <p>
                {JSON.parse(localStorage.getItem("LastName"))}
                {JSON.parse(localStorage.getItem("FirstName"))}
              </p>
            </strong>
            <strong>BİREYSEL ABONE İMZA</strong>
          </div>
          <div className="col-md-6 text-right">
            <strong style={{ fontSize: 14 }}>MYFİ ADINA</strong>
            <br />
            <strong>KAŞE İMZA</strong>
          </div>
        </div>
      </div>
    );
  }
}
