import React, { Component, useState } from "react";
import { Button, Form, InputGroup, Col, Modal } from "react-bootstrap";
import * as jsPDF from "jspdf";
import $, { jQuery } from "jquery";
import DatePicker from "react-datepicker";
import { Redirect, Link } from "react-router-dom";

import { BrowserRouter as Router } from "react-router-dom";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import axios from "axios";
import countries from "../data/countries";

import logo2 from "./logo2.jpg";

import "react-datepicker/dist/react-datepicker.css";
import newcontract from "./newcontract";

export class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      idcard: "",
      taxid: "",
      passport: "",
      gender: "",
      status: 0,
      tc: "",
      fatherName: "",
      motherName: "",
      birthDay: "",
      birthPlace: "",
      kindofService: "",
      postCode: "",
      servicePacket: [],
      staticIp: "",
      country: "",
      city: "",
      district: "",
      neighborhood: "",
      streetNumber: "",
      street: "",
      adresse: "",
      phoneNumber: "",
      gsmNumber: "",
      email: "",
      userName: "",
      password: "",
      clientId: 0,
      idcardpicture: "",
      dateidcard: "",
      stationinfoName: "",
      number: 32,
      page: "",
      serienumber: "",
      place: "",
      latitude: 32,
      longitude: 32,
      numPages: null,
      pageNumber: 1,
      loading: true,
      socialStatus: "",
      servicePackets: [],
      contractid: ""
    };

    this.handleBirthdayDateChange = this.handleBirthdayDateChange.bind(this);
    this.handleIdCardDateChange = this.handleIdCardDateChange.bind(this);
    this.handleCreateTimeDateChange = this.handleCreateTimeDateChange.bind(
      this
    );

    // fetch("http://95.0.46.234:5454/api/GetContractId/{userName}")
    //   .then(results => results.json())
    //   .then(json => {
    //     this.state.contractid = json;
    //     console.log(json);
    //   });

    //this.handleEditTimeDateChange = this.handleEditTimeDateChange.bind(this);
    //this.validationCheck = this.validationCheck.bind(this);
  }

  componentDidMount(props) {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;

        this.setState({
          longitude: longitude,
          latitude: latitude,
          loading: false
        });
      },
      () => {
        this.setState({ loading: false });
      }
    );

    fetch("http://95.0.46.234:5454/api/InternetPackages")
      .then(results => results.json())
      .then(json => {
        console.log(json);
        this.setState({ servicePackets: json });
      });
  }

  handleBirthdayDateChange(date) {
    this.setState({ birthDay: date });
  }

  handleIdCardDateChange(date) {
    this.setState({ dateidcard: date });
  }

  handleCreateTimeDateChange(date) {
    this.setState({ formCreatetime: date });
  }
  handleCreateAdresse(street, streetNumber) {
    this.setState({ street } + { streetNumber });
  }
  // redirectToTarget = () => {
  //   this.context.router.history.push(`/newcontract`);
  // };

  // handleEditTimeDateChange(date) {
  //   this.setState({ formEdittime: date });
  // }
  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/con" />;
    }
  };

  handleClientIdChange = e => {
    this.setState(
      {
        clientId: this.state.serienumber
      },
      () => console.log(this.state)
    );
  };
  getCurrentDate = e => {
    var separator = "";

    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();

    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date}`;
  };
  handleKindOfServiceChange = e => {
    if (e.target.value == "Corporate") {
      this.setState(
        {
          userName: this.state.taxid,
          kindofService: e.target.value
        },
        () => console.log(this.state)
      );
    } else if (e.target.value == "Personal") {
      this.setState(
        {
          userName: this.state.idcard,
          kindofService: e.target.value
        },
        () => console.log(this.state)
      );
    }
  };
  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // printSubmitMe = e => {
  //   window.print();
  // };
  handleInputFile = e => {
    let files = e.target.files;

    let reader = new FileReader();
    reader.readAsDataURL(files[0]);

    reader.onload = e => {
      this.setState({ idcardpicture: e.target.result });
    };
  };
  onChangelocalStorage = event => {
    localStorage.setItem("Email", JSON.stringify(this.state.email));
    localStorage.setItem("LastName", JSON.stringify(this.state.lastName));
    localStorage.setItem("FirstName", JSON.stringify(this.state.firstName));
    localStorage.setItem("Telephone", JSON.stringify(this.state.phoneNumber));
    localStorage.setItem("GSM", JSON.stringify(this.state.gsmNumber));
    localStorage.setItem("poste", JSON.stringify(this.state.postCode));
    localStorage.setItem("txid", JSON.stringify(this.state.taxid));
    localStorage.setItem("idcard", JSON.stringify(this.state.idcard));
    localStorage.setItem("passport", JSON.stringify(this.state.passport));
    localStorage.setItem("status", JSON.stringify(this.state.socialStatus));
    localStorage.setItem("district", JSON.stringify(this.state.district));
    localStorage.setItem("tc", JSON.stringify(this.state.tc));
    localStorage.setItem("gender", JSON.stringify(this.state.gender));

    localStorage.setItem(
      "streetnumber",
      JSON.stringify(this.state.streetNumber)
    );

    var servicePacketsSelect = document.getElementById("servicePacket");
    var selectedServicePacket =
      servicePacketsSelect.options[servicePacketsSelect.selectedIndex].id;
    console.log(selectedServicePacket);
    localStorage.setItem(
      "servicePacket",
      JSON.stringify(selectedServicePacket)
    );

    var userNameSelect = document.getElementById("userName");
    var selectedUserName =
      userNameSelect.options[userNameSelect.selectedIndex].contactId;
    console.log(selectedUserName);
    localStorage.setItem("servicePacket", JSON.stringify(selectedUserName));

    localStorage.setItem("street", JSON.stringify(this.state.street));

    localStorage.setItem(
      "neighbrhood",
      JSON.stringify(this.state.neighborhood)
    );
    localStorage.setItem("txid", JSON.stringify(this.state.taxid));

    localStorage.setItem("contractid", JSON.stringify(this.state.contractid));

    localStorage.setItem("Adress", JSON.stringify(this.state.streetNumber));
    // window.print(newcontract);
  };

  onSubmithandler = event => {
    //console.log(this.state);

    event.preventDefault();

    this.state.adresse =
      this.state.streetNumber +
      " " +
      this.state.street +
      "  , " +
      this.state.neighborhood +
      ", " +
      "" +
      this.state.district +
      " , " +
      this.state.city +
      "," +
      this.state.postCode;
    var formData = JSON.parse(JSON.stringify(this.state));
    delete formData.loading;
    delete formData.servicePackets;

    fetch("http://95.0.46.234:5454/api/User", {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept"
      }
    })
      .then(res => {
        if (res.status === 200) {
          this.props.history.push("/");

          //alert("Success");
        } else {
          const error = new Error(res.error);
          throw error;
        }
      })
      .catch(err => {
        console.error(err);
        alert("Error please try again");
      });
  };

  onGetPackagehandler = event => {};

  render() {
    const {
      firstName,
      lastName,
      idcard,
      taxid,
      passport,
      gender,
      status,
      tc,
      fatherName,
      motherName,
      kindofService,
      postCode,
      birthDay,
      birthPlace,
      country,
      city,
      adresse,
      district,
      neighborhood,
      streetNumber,
      street,
      phoneNumber,
      gsmNumber,
      email,
      userName,
      password,
      idcardpicture,
      dateidcard,
      number,
      staticIp,
      clientId,
      stationinfoName,
      page,
      serienumber,
      servicePacket,
      place,
      latitude,
      longitude,
      socialStatus,
      contractid,
      numPages
    } = this.state;
    const Location = () => {
      const { loading } = this.state;
      const { google } = this.props;
      const mapStyle = {
        width: "100%",
        height: "500px",
        position: "relative"
      };
      const [show, setShow] = useState(false);

      const handleClose = () => setShow(false);
      const handleShow = () => setShow(true);

      return (
        <>
          <Button variant="primary" className="mt-4" onClick={handleShow}>
            Location map
          </Button>

          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Your location</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {" "}
              {loading ? null : (
                <Map
                  google={google}
                  containerStyle={mapStyle}
                  initialCenter={{ lat: latitude, lng: longitude }}
                  zoom={10}
                >
                  <Marker
                    onClick={this.onMarkerClick}
                    name={"Current location"}
                  />
                </Map>
              )}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      );
    };
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="aside col-md-2 py-5 bg-primary  text-white text-center ">
            <div className=" ">
              <div className="card-body">
                <img src={logo2} alt="logo" style={{ width: "100%" }} />
              </div>
            </div>
          </div>

          <div className="col-md-10 py-3 border">
            <h4 className="pb-2">Lütfen bilgilerinizi giriniz</h4>
            <p className="h5 pb-2 text-secondary">Kişisel bilgi</p>
            <Form onSubmit={this.onSubmithandler}>
              <Form.Row>
                <Form.Group as={Col} md="4">
                  <Form.Label>İsim</Form.Label>
                  <Form.Control
                    placeholder="İsim"
                    type="text"
                    name="firstName"
                    onChange={this.handleInputChange}
                    value={firstName}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label htmlFor="lastName">Soyadı</Form.Label>
                  <Form.Control
                    placeholder="Soyadı"
                    type="text"
                    name="lastName"
                    onChange={this.handleInputChange}
                    value={this.state.lastName}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Cinsiyet</Form.Label>
                  <Form.Control
                    as="select"
                    name="gender"
                    onChange={this.handleInputChange}
                    value={gender}
                    required
                  >
                    <option />
                    <option
                      key="erkek
"
                    >
                      erkek
                    </option>
                    <option
                      key="kadın
"
                    >
                      kadın
                    </option>
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} md="2">
                  <Form.Label>Sosyal durum</Form.Label>
                  <Form.Control
                    as="select"
                    name="socialStatus"
                    onChange={this.handleInputChange}
                    value={socialStatus}
                    required
                  >
                    <option />
                    <option key="Student">Öğrenci</option>
                    <option key="worker">Çalışan</option>
                    <option key="freelance">Serbest</option>
                    <option key="unemployed">İşsiz</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group as={Col} md="2">
                  <Form.Label>Doğum günü</Form.Label>

                  <DatePicker
                    selected={birthDay}
                    className="form-control"
                    onChange={this.handleBirthdayDateChange}
                    required
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="4">
                  <Form.Label htmlFor="fatherName">Baba adı</Form.Label>
                  <Form.Control
                    placeholder="Baba adı"
                    type="text"
                    name="fatherName"
                    onChange={this.handleInputChange}
                    value={fatherName}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Anne adı</Form.Label>
                  <Form.Control
                    placeholder="Anne adı
                    "
                    type="text"
                    name="motherName"
                    onChange={this.handleInputChange}
                    value={motherName}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>doğum yeri</Form.Label>
                  <Form.Control
                    placeholder="doğum yeri
                    "
                    type="text"
                    name="birthPlace"
                    onChange={this.handleInputChange}
                    value={birthPlace}
                    required
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>ID kart numarası</Form.Label>
                  <Form.Control
                    placeholder="ID kart numarası
                    "
                    type="number"
                    name="idcard"
                    onChange={this.handleInputChange}
                    value={idcard}
                    required
                  />
                </Form.Group>

                <Form.Group as={Col} md="3">
                  <Form.Label>Seri Numarası</Form.Label>
                  <Form.Control
                    placeholder="Seri Numarası
                    "
                    type="text"
                    name="serienumber"
                    onChange={this.handleInputChange}
                    value={serienumber}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Pasaport numarası</Form.Label>
                  <Form.Control
                    placeholder="Pasaport numarası
                    "
                    type="text"
                    name="passport"
                    onChange={this.handleInputChange}
                    value={passport}
                    //required
                  />
                </Form.Group>

                <Form.Group as={Col} md="3">
                  <Form.Label>Tarih Kimlik Kartı</Form.Label>
                  <DatePicker
                    selected={dateidcard}
                    className="form-control"
                    onChange={this.handleIdCardDateChange}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Sayfa</Form.Label>
                  <Form.Control
                    placeholder="Sayfa
                    "
                    type="text"
                    name="page"
                    onChange={this.handleInputChange}
                    value={page}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Kimlik kartı resmi</Form.Label>
                  <Form.Control
                    placeholder="Kimlik kartı resmi
                    "
                    type="file"
                    name="idcardpicture"
                    onChange={this.handleInputFile}
                    //required
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>Vergi numarası</Form.Label>
                  <Form.Control
                    placeholder="Vergi numarası
                    "
                    type="number"
                    name="taxid"
                    onChange={this.handleInputChange}
                    value={taxid}
                    //required
                  />
                </Form.Group>

                <Form.Group as={Col} md="3">
                  <Form.Label>kimlik tc</Form.Label>
                  <Form.Control
                    placeholder="kimlik tc
                    "
                    type="text"
                    name="tc"
                    onChange={this.handleInputChange}
                    value={tc}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Hizmet türü</Form.Label>
                  <Form.Control
                    as="select"
                    name="kindofService"
                    onChange={this.handleKindOfServiceChange}
                    value={kindofService}
                    //required
                  >
                    <option />
                    <option key="corporate">Kurumsal</option>
                    <option key="personal">Kişisel</option>
                  </Form.Control>
                </Form.Group>
              </Form.Row>
              <p className="h5 py-3 text-secondary">İletişim bilgileri</p>
              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>Telefon numarası</Form.Label>
                  <Form.Control
                    placeholder="Telefon numarası
                    "
                    type="tel"
                    name="phoneNumber"
                    onChange={this.handleInputChange}
                    value={phoneNumber}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>GSM Numarası</Form.Label>
                  <Form.Control
                    placeholder="GSM Numarası
                    "
                    type="tel"
                    name="gsmNumber"
                    onChange={this.handleInputChange}
                    value={gsmNumber}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    placeholder="Email"
                    type="email"
                    name="email"
                    onChange={this.handleInputChange}
                    value={email}
                    //required
                  />
                </Form.Group>
              </Form.Row>
              <p className="h5 py-3 text-secondary">Adres bilgisi</p>
              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>ülke</Form.Label>
                  <Form.Control
                    as="select"
                    name="country"
                    onChange={this.handleInputChange}
                    defaultValue="Turkey"
                    required
                  >
                    <option />
                    {countries.map(c => (
                      <option key={c.code}>{c.name}</option>
                    ))}
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Kent</Form.Label>
                  <Form.Control
                    placeholder="Kent"
                    type="text"
                    name="city"
                    onChange={this.handleInputChange}
                    defaultValue="Sakarya"
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>İlçe</Form.Label>
                  <Form.Control
                    placeholder="İlçe"
                    type="text"
                    name="district"
                    onChange={this.handleInputChange}
                    value={district}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>İlçe</Form.Label>
                  <Form.Control
                    placeholder="İlçe"
                    type="text"
                    name="neighborhood"
                    onChange={this.handleInputChange}
                    value={neighborhood}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Sokak numarası</Form.Label>
                  <Form.Control
                    placeholder="Sokak numarası
                    "
                    type="text"
                    name="streetNumber"
                    onChange={this.handleInputChange}
                    value={streetNumber}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>sokak</Form.Label>
                  <Form.Control
                    placeholder="sokak"
                    type="text"
                    name="street"
                    onChange={this.handleInputChange}
                    value={street}
                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Posta Kodu</Form.Label>
                  <Form.Control
                    placeholder="Posta Kodu
                    "
                    type="number"
                    name="postCode"
                    onChange={this.handleInputChange}
                    value={postCode}
                    //required
                  />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} md="4">
                  <Form.Label>boylam</Form.Label>

                  <Form.Control
                    placeholder="longitude"
                    type="text"
                    name="longitude"
                    onChange={this.handleInputChange}
                    value={this.state.longitude}

                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Form.Label>Enlem</Form.Label>

                  <Form.Control
                    placeholder="latitude"
                    type="text"
                    name="latitude"
                    onChange={this.handleInputChange}
                    value={this.state.latitude}

                    //required
                  />
                </Form.Group>
                <Form.Group as={Col} md="4">
                  <Location />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} md="4">
                  <Form.Label>Adres</Form.Label>
                  <Form.Control
                    placeholder="Açık adres
                    "
                    type="text"
                    name="adresse"
                    readOnly
                    value={
                      this.state.streetNumber +
                      "" +
                      this.state.street +
                      "  , " +
                      this.state.neighborhood +
                      ", " +
                      "" +
                      this.state.district +
                      " , " +
                      this.state.city +
                      "," +
                      this.state.postCode
                    }
                    required
                  />
                </Form.Group>
              </Form.Row>

              <p className="h5 py-3 text-secondary">Abonelik Bilgileri</p>
              <Form.Row>
                <Form.Group as={Col} md="4">
                  <Form.Label>Servis Paketi</Form.Label>

                  <Form.Control
                    id="servicePacket"
                    as="select"
                    name="servicePacket"
                    onChange={this.handleInputChange}
                    //required
                  >
                    <option />
                    {this.state.servicePackets.map(service => (
                      <option key={service.packid} id={service.packid}>
                        {service.packName}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>{" "}
                <Form.Group as={Col} md="3">
                  <Form.Label>Kullanıcı adı</Form.Label>

                  <InputGroup>
                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputGroupPrepend">
                        @
                      </InputGroup.Text>
                    </InputGroup.Prepend>

                    <Form.Control
                      id="userName"
                      type="text"
                      placeholder="Username"
                      name="userName"
                      onChange={e => e.preventDefault()}
                      value={this.state.userName}
                      aria-describedby="inputGroupPrepend"
                      readOnly
                      //required
                    />
                    <Form.Control.Feedback type="invalid">
                      Please choose a username.
                    </Form.Control.Feedback>
                  </InputGroup>
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    placeholder="Password"
                    type="password"
                    name="password"
                    onChange={this.handleInputChange}
                    value={this.state.idcard}
                    required
                  />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>Müşteri Kimliği</Form.Label>
                  <Form.Control
                    placeholder="Müşteri Kimliği
                    "
                    type="text"
                    name="clientId"
                    onChange={this.handleInputChange}
                    value={this.state.clientId}
                    required
                  />
                </Form.Group>
                <Form.Group as={Col} md="3">
                  <Form.Label>Statik IP</Form.Label>
                  <Form.Control
                    placeholder="Statik IP
                    "
                    type="text"
                    name="staticIp"
                    onChange={this.handleInputChange}
                    value={staticIp}
                    //required
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="3">
                  <Form.Label>İstasyon Bilgisi Adı</Form.Label>
                  <Form.Control
                    placeholder="İstasyon Bilgisi Adı
                    "
                    type="text"
                    name="stationinfoName"
                    onChange={this.handleInputChange}
                    value={stationinfoName}
                    //required
                  />
                </Form.Group>
              </Form.Row>
              {/* onClick={this.setRedirect} */}
              {/* {this.renderRedirect()} */}

              <Button type="submit">Formu gönder</Button>
              {/* <Button type="button" onChange={this.onChangelocalStorage}  */}
              <Link to="/co">
                <Button type="button" onClick={this.onChangelocalStorage}>
                  Contract{" "}
                </Button>
              </Link>
              {/* <Button onClick={this.onDocumentLoadSuccess}>Download PDF</Button> */}
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyAwhlqEYzNTr0riMy0n79bigyouAcW_G48"
})(Subscription);
